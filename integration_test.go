package main_test

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	gitalyClient "gitlab.com/gitlab-org/gitaly/v16/client"
	pb "gitlab.com/gitlab-org/gitaly/v16/proto/go/gitalypb"
	"gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/elastic"
	"gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/indexer"
)

var (
	binary         = flag.String("binary", "./bin/gitlab-elasticsearch-indexer", "Path to `gitlab-elasticsearch-indexer` binary for integration tests")
	gitalyConnInfo *gitalyConnectionInfo
)

const (
	groupID               = 1
	projectID             = 667
	projectIDString       = "667"
	groupIDString         = "1"
	headSHA               = "b83d6e391c22777fca1ed3012fce84f633d7fed0"
	testRepo              = "test-gitlab-elasticsearch-indexer/gitlab-test.git"
	testRepoPath          = "https://gitlab.com/gitlab-org/gitlab-test.git"
	visibilityLevel       = int8(10)
	repositoryAccessLevel = int8(20)
	hashedRootNamespaceId = int16(63)
	traversalId           = "2-1-"
	namespaceIdString     = "2"
	schemaVersionBlob     = float64(23_08)
	schemaVersionCommit   = float64(23_06)
	schemaVersionWiki     = float64(23_08)
)

type gitalyConnectionInfo struct {
	Address string `json:"address"`
	Storage string `json:"storage"`
}

func validProjectPermissions() *indexer.ProjectPermissions {
	return &indexer.ProjectPermissions{
		VisibilityLevel:       visibilityLevel,
		RepositoryAccessLevel: repositoryAccessLevel,
	}
}

func init() {
	gci, exists := os.LookupEnv("GITALY_CONNECTION_INFO")
	if exists {
		err := json.Unmarshal([]byte(gci), &gitalyConnInfo)
		if err != nil {
			panic(err)
		}
	}
}

func TestIndexingRenamesFiles(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")

	defer td()

	// The commit before files/js/commit.js.coffee is renamed
	err, _ := run("", "281d3a76f31c812dbf48abce82ccf6860adedd81", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)
	_, err = c.GetBlob("files/js/commit.js.coffee")
	require.NoError(t, err)

	// Now we expect it to have been renamed
	err, _ = run("281d3a76f31c812dbf48abce82ccf6860adedd81", "c347ca2e140aa667b968e51ed0ffe055501fe4f4", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)
	_, err = c.GetBlob("files/js/commit.js.coffee")
	require.Error(t, err)
	_, err = c.GetBlob("files/js/commit.coffee")
	require.NoError(t, err)
}

func ensureGitalyRepository(t *testing.T) {
	conn, err := gitalyClient.Dial(gitalyConnInfo.Address, gitalyClient.DefaultDialOpts)
	require.NoError(t, err)

	repository := pb.NewRepositoryServiceClient(conn)
	// Remove repository for consistency
	if _, err = repository.RemoveRepository(context.Background(), &pb.RemoveRepositoryRequest{
		Repository: &pb.Repository{
			StorageName:  gitalyConnInfo.Storage,
			RelativePath: testRepo,
		},
	}); err != nil {
		removeRepositoryStatus, ok := status.FromError(err)
		if !ok || !(removeRepositoryStatus.Code() == codes.NotFound && removeRepositoryStatus.Message() == "repository does not exist") {
			require.NoError(t, err)
		}

		// Repository didn't exist.
	}

	glRepository := &pb.Repository{StorageName: gitalyConnInfo.Storage, RelativePath: testRepo}
	createReq := &pb.CreateRepositoryFromURLRequest{Repository: glRepository, Url: testRepoPath}

	_, err = repository.CreateRepositoryFromURL(context.Background(), createReq)
	require.NoError(t, err)
}

func checkDeps(t *testing.T) {
	if os.Getenv("ELASTIC_CONNECTION_INFO") == "" {
		t.Skip("ELASTIC_CONNECTION_INFO not set")
	}

	if os.Getenv("GITALY_CONNECTION_INFO") == "" {
		t.Skip("GITALY_CONNECTION_INFO is not set")
	}

	if testing.Short() {
		t.Skip("Test run with -short, skipping integration test")
	}

	if _, err := os.Stat(*binary); err != nil {
		t.Skip("No binary found at ", *binary)
	}
}

func groupIdOption() string {
	return fmt.Sprintf("--group-id=%d", groupID)
}

func projectIdOption() string {
	return fmt.Sprintf("--project-id=%d", projectID)
}

func schemaVersionBlobValueOption() string {
	return fmt.Sprintf("--schema-version-blob=%v", schemaVersionBlob)
}

func schemaVersionCommitValueOption() string {
	return fmt.Sprintf("--schema-version-commit=%v", schemaVersionCommit)
}

func schemaVersionWikiValueOption() string {
	return fmt.Sprintf("--schema-version-wiki=%v", schemaVersionWiki)
}

func archivedOption(archived bool) string {
	return fmt.Sprintf("--archived=%t", archived)
}

func buildWorkingIndex(t *testing.T, routing string) (*elastic.Client, func()) {
	return buildIndex(t, true, routing)
}

func buildBrokenIndex(t *testing.T) (*elastic.Client, func()) {
	return buildIndex(t, false, "project")
}

func buildIndex(t *testing.T, working bool, routing string) (*elastic.Client, func()) {
	originalEnvVar := os.Getenv("ELASTIC_CONNECTION_INFO")

	setElasticsearchConnectionInfo(t)

	config, err := elastic.ConfigFromEnv()
	require.NoError(t, err)

	config.Permissions = validProjectPermissions()

	if routing == "project" {
		config.ProjectID = projectID
	} else {
		config.GroupID = groupID
	}
	client, err := elastic.NewClient(config, "the-correlation-id")
	require.NoError(t, err)

	if working {
		require.NoError(t, client.CreateDefaultWorkingIndex())
		require.NoError(t, client.CreateCommitsWorkingIndex())
		require.NoError(t, client.CreateWikisWorkingIndex())
	} else {
		require.NoError(t, client.CreateDefaultBrokenIndex())
	}

	return client, func() {
		if err = os.Setenv("ELASTIC_CONNECTION_INFO", originalEnvVar); err != nil {
			require.NoError(t, err)
		}

		if err = client.DeleteIndex(client.IndexNameDefault); err != nil {
			require.NoError(t, err)
		}

		if working {
			if err = client.DeleteIndex(client.IndexNameCommits); err != nil {
				require.NoError(t, err)
			}
		}

		if working {
			if err = client.DeleteIndex(client.IndexNameWikis); err != nil {
				require.NoError(t, err)
			}
		}
	}
}

// Substitute index_name with a dynamically generated one
func setElasticsearchConnectionInfo(t *testing.T) {
	config, err := elastic.ReadConfig(strings.NewReader(os.Getenv("ELASTIC_CONNECTION_INFO")))
	require.NoError(t, err)

	config.IndexNameDefault = fmt.Sprintf("%s-%d", config.IndexNameDefault, time.Now().Unix())
	config.IndexNameCommits = fmt.Sprintf("%s-commits-%d", config.IndexNameDefault, time.Now().Unix())
	config.IndexNameWikis = fmt.Sprintf("%s-wikis-%d", config.IndexNameDefault, time.Now().Unix())

	out, err := json.Marshal(config)
	require.NoError(t, err)

	if err = os.Setenv("ELASTIC_CONNECTION_INFO", string(out)); err != nil {
		require.NoError(t, err)
	}
}

func run(from, to string, args ...string) (error, string) {
	var (
		stdout          bytes.Buffer
		stderr          bytes.Buffer
		fromSHAFlag     string
		toSHAFlag       string
		traversalIdFlag string
	)

	// GitLab always sets FROM_SHA
	if from == "" {
		from = "0000000000000000000000000000000000000000"
	}
	fromSHAFlag = fmt.Sprintf("--from-sha=%s", from)
	if to != "" {
		toSHAFlag = fmt.Sprintf("--to-sha=%s", to)
	}

	traversalIdFlag = fmt.Sprintf("--traversal-ids=%s", traversalId)
	arguments := append(args, fromSHAFlag, toSHAFlag, traversalIdFlag, testRepo)
	cmd := exec.Command(*binary, arguments...)
	cmd.Env = os.Environ()
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()

	return err, stdout.String()
}

func TestIndexingRemovesFiles(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")

	defer td()

	// The commit before files/empty is removed - so it should be indexed
	err, _ := run("", "19e2e9b4ef76b422ce1154af39a91323ccc57434", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)
	_, err = c.GetBlob("files/empty")
	require.NoError(t, err)

	// Now we expect it to have been removed
	err, _ = run("19e2e9b4ef76b422ce1154af39a91323ccc57434", "08f22f255f082689c0d7d39d19205085311542bc", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)
	_, err = c.GetBlob("files/empty")
	require.Error(t, err)
}

func TestIndexingTimeout(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	_, td := buildWorkingIndex(t, "project")

	defer td()

	err, stdout := run("", "e2c7507b72f55cc272bbd5fde5bfa46eb4aeeebf", "--timeout=0s", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())

	require.Error(t, err)
	require.Regexp(t, `The process has timed out`, stdout)
	err, stdout = run("", "e2c7507b72f55cc272bbd5fde5bfa46eb4aeeebf", "--timeout=100", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())

	require.Error(t, err)
	require.Regexp(t, `time: missing unit in duration`, stdout)
}

func TestIndexingFilesWithLongPath(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")

	defer td()

	// commit where a filename with a very long path is introduced
	err, _ := run("", "e2c7507b72f55cc272bbd5fde5bfa46eb4aeeebf", schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)

	// hashed filename from the commit above
	_, err = c.GetBlob("e600da5384d3a616836c47fb8df1a84ce7752bd3")
	require.NoError(t, err)
}

func TestIndexingBlobWithoutSchemaVersionBlobValueOption(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	_, td := buildWorkingIndex(t, "project")

	defer td()

	err, stdout := run("", headSHA, schemaVersionCommitValueOption(), projectIdOption())
	require.Error(t, err)

	require.Regexp(t, `Error: schemaVersionBlob is empty`, stdout)
}

func TestIndexingWikiWithoutSchemaVersionWikiValueOption(t *testing.T) {

	checkDeps(t)
	ensureGitalyRepository(t)
	_, td := buildWorkingIndex(t, "project")
	defer td()

	err, stdout := run("", headSHA, "--blob-type=wiki_blob", schemaVersionCommitValueOption(), groupIdOption(), projectIdOption(), "--skip-commits")
	require.Error(t, err)

	require.Regexp(t, `Error: schemaVersionWiki is empty`, stdout)
}

type document struct {
	Blob      *indexer.Blob     `json:"blob"`
	Commit    *indexer.Commit   `json:"commit"`
	Type      string            `json:"string"`
	JoinField map[string]string `json:"join_field"`
}

// Go source is defined to be UTF-8 encoded, so literals here are UTF-8
func TestIndexingTranscodesToUTF8(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	err, _ := run("", headSHA, schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)

	for _, tc := range []struct {
		name     string
		path     string
		expected string
	}{
		{"GB18030", "encoding/iso8859.txt", "狞\n"},
		{"SHIFT_JIS", "encoding/test.txt", "これはテストです。\nこれもマージして下さい。\n\nAdd excel file.\nDelete excel file."},
	} {
		t.Run(tc.name, func(t *testing.T) {
			blob, err := c.GetBlob(tc.path)
			require.NoError(t, err)

			blobDoc := &document{}
			require.NoError(t, json.Unmarshal(blob.Source, &blobDoc))

			require.Equal(t, tc.expected, blobDoc.Blob.Content)
		})
	}
}

func TestElasticClientIndexMismatch(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	_, td := buildBrokenIndex(t)
	defer td()

	err, stdout := run("", headSHA, schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())

	require.Error(t, err)
	require.Regexp(t, `Bulk request failed to insert \d/\d documents`, stdout)
}

func TestIndexingGitlabTestWithArchivedFlag(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	err, _ := run("", headSHA, projectIdOption(), archivedOption(false), schemaVersionBlobValueOption(), schemaVersionCommitValueOption())
	require.NoError(t, err)

	// Check the indexing of a text blob
	blob, err := c.GetBlob("README.md")
	require.NoError(t, err)
	require.True(t, blob.Found)
	require.Equal(t, projectIDString+"_README.md", blob.Id)
	require.Equal(t, "project_"+projectIDString, blob.Routing)

	var data = make(map[string]interface{})
	require.NoError(t, json.Unmarshal(blob.Source, &data))

	blobDoc, ok := data["blob"]
	require.True(t, ok)
	archived, ok := data["archived"]
	require.True(t, ok)
	schemaVersion, ok := data["schema_version"]
	require.True(t, ok)
	require.False(t, archived.(bool))
	require.InDelta(t, schemaVersionBlob, schemaVersion, 0)
	require.Equal(
		t,
		map[string]interface{}{
			"type":       "blob",
			"language":   "Markdown",
			"path":       "README.md",
			"file_name":  "README.md",
			"oid":        "faaf198af3a36dbf41961466703cc1d47c61d051",
			"rid":        projectIDString,
			"commit_sha": headSHA,
			"content":    "testme\n======\n\nSample repo for testing gitlab features\n",
		},
		blobDoc,
	)

	// Check that a binary blob is indexed
	blob, err = c.GetBlob("Gemfile.zip")
	require.NoError(t, err)
	require.True(t, blob.Found)
	require.Equal(t, projectIDString+"_Gemfile.zip", blob.Id)
	require.Equal(t, "project_"+projectIDString, blob.Routing)
}

func TestIndexingGitlabTest(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	err, _ := run("", headSHA, projectIdOption(), schemaVersionBlobValueOption(), schemaVersionCommitValueOption())
	require.NoError(t, err)

	// Check the indexing of a text blob
	blob, err := c.GetBlob("README.md")
	require.NoError(t, err)
	require.True(t, blob.Found)
	require.Equal(t, projectIDString+"_README.md", blob.Id)
	require.Equal(t, "project_"+projectIDString, blob.Routing)

	var data = make(map[string]interface{})
	require.NoError(t, json.Unmarshal(blob.Source, &data))

	blobDoc, ok := data["blob"]
	require.True(t, ok)
	require.Equal(
		t,
		map[string]interface{}{
			"type":       "blob",
			"language":   "Markdown",
			"path":       "README.md",
			"file_name":  "README.md",
			"oid":        "faaf198af3a36dbf41961466703cc1d47c61d051",
			"rid":        projectIDString,
			"commit_sha": headSHA,
			"content":    "testme\n======\n\nSample repo for testing gitlab features\n",
		},
		blobDoc,
	)

	// Check that a binary blob is indexed
	blob, err = c.GetBlob("Gemfile.zip")
	require.NoError(t, err)
	require.True(t, blob.Found)
	require.Equal(t, projectIDString+"_Gemfile.zip", blob.Id)
	require.Equal(t, "project_"+projectIDString, blob.Routing)
	schemaVersion, ok := data["schema_version"]
	require.True(t, ok)
	require.InDelta(t, schemaVersionBlob, schemaVersion, 0)
}

func TestIndexingCommitsWithArchivedFlagFalse(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	hashedRootNamespaceIdFlag := fmt.Sprintf("--hashed-root-namespace-id=%d", hashedRootNamespaceId)
	err, _ := run("", headSHA, hashedRootNamespaceIdFlag, projectIdOption(), schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), archivedOption(false))
	require.NoError(t, err)

	// Check the indexing of a commit
	commit, err := c.GetCommit(headSHA)
	require.NoError(t, err)
	require.True(t, commit.Found)
	require.Equal(t, projectIDString+"_"+headSHA, commit.Id)
	require.Equal(t, "project_"+projectIDString, commit.Routing)

	commitDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(commit.Source, &commitDoc))

	date, err := time.Parse("20060102T150405-0700", "20160927T143746+0000")
	require.NoError(t, err)

	require.Equal(
		t,
		map[string]interface{}{
			"type": "commit",
			"sha":  headSHA,
			"author": map[string]interface{}{
				"email": "job@gitlab.com",
				"name":  "Job van der Voort",
				"time":  date.Local().Format("20060102T150405-0700"),
			},
			"committer": map[string]interface{}{
				"email": "job@gitlab.com",
				"name":  "Job van der Voort",
				"time":  date.Local().Format("20060102T150405-0700"),
			},
			"rid":                      projectIDString,
			"schema_version":           schemaVersionCommit,
			"archived":                 false,
			"hashed_root_namespace_id": float64(hashedRootNamespaceId), // float64 cast needed due to: https://github.com/olivere/elastic/issues/891
			"message":                  "Merge branch 'branch-merged' into 'master'\r\n\r\nadds bar folder and branch-test text file to check Repository merged_to_root_ref method\r\n\r\n\r\n\r\nSee merge request !12",
		},
		commitDoc,
	)
}

func TestIndexingCommitsWithArchivedFlagTrue(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	hashedRootNamespaceIdFlag := fmt.Sprintf("--hashed-root-namespace-id=%d", hashedRootNamespaceId)
	err, _ := run("", headSHA, hashedRootNamespaceIdFlag, projectIdOption(), schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), archivedOption(true))
	require.NoError(t, err)

	// Check the indexing of a commit
	commit, err := c.GetCommit(headSHA)
	require.NoError(t, err)
	require.True(t, commit.Found)
	require.Equal(t, projectIDString+"_"+headSHA, commit.Id)
	require.Equal(t, "project_"+projectIDString, commit.Routing)

	commitDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(commit.Source, &commitDoc))

	date, err := time.Parse("20060102T150405-0700", "20160927T143746+0000")
	require.NoError(t, err)

	require.Equal(
		t,
		map[string]interface{}{
			"type": "commit",
			"sha":  headSHA,
			"author": map[string]interface{}{
				"email": "job@gitlab.com",
				"name":  "Job van der Voort",
				"time":  date.Local().Format("20060102T150405-0700"),
			},
			"committer": map[string]interface{}{
				"email": "job@gitlab.com",
				"name":  "Job van der Voort",
				"time":  date.Local().Format("20060102T150405-0700"),
			},
			"rid":                      projectIDString,
			"schema_version":           schemaVersionCommit,
			"archived":                 true,
			"hashed_root_namespace_id": float64(hashedRootNamespaceId), // float64 cast needed due to: https://github.com/olivere/elastic/issues/891
			"message":                  "Merge branch 'branch-merged' into 'master'\r\n\r\nadds bar folder and branch-test text file to check Repository merged_to_root_ref method\r\n\r\n\r\n\r\nSee merge request !12",
		},
		commitDoc,
	)
}

func TestIndexingCommits(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	hashedRootNamespaceIdFlag := fmt.Sprintf("--hashed-root-namespace-id=%d", hashedRootNamespaceId)
	err, _ := run("", headSHA, hashedRootNamespaceIdFlag, schemaVersionBlobValueOption(), schemaVersionCommitValueOption(), projectIdOption())
	require.NoError(t, err)

	// Check the indexing of a commit
	commit, err := c.GetCommit(headSHA)
	require.NoError(t, err)
	require.True(t, commit.Found)
	require.Equal(t, projectIDString+"_"+headSHA, commit.Id)
	require.Equal(t, "project_"+projectIDString, commit.Routing)

	commitDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(commit.Source, &commitDoc))

	date, err := time.Parse("20060102T150405-0700", "20160927T143746+0000")
	require.NoError(t, err)

	require.Equal(
		t,
		map[string]interface{}{
			"type": "commit",
			"sha":  headSHA,
			"author": map[string]interface{}{
				"email": "job@gitlab.com",
				"name":  "Job van der Voort",
				"time":  date.Local().Format("20060102T150405-0700"),
			},
			"committer": map[string]interface{}{
				"email": "job@gitlab.com",
				"name":  "Job van der Voort",
				"time":  date.Local().Format("20060102T150405-0700"),
			},
			"rid":                      projectIDString,
			"schema_version":           schemaVersionCommit,
			"hashed_root_namespace_id": float64(hashedRootNamespaceId), // float64 cast needed due to: https://github.com/olivere/elastic/issues/891
			"message":                  "Merge branch 'branch-merged' into 'master'\r\n\r\nadds bar folder and branch-test text file to check Repository merged_to_root_ref method\r\n\r\n\r\n\r\nSee merge request !12",
		},
		commitDoc,
	)
}

func TestIndexingProjectWikiBlobs(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	err, _ := run("", headSHA, "--blob-type=wiki_blob", groupIdOption(), projectIdOption(), schemaVersionWikiValueOption(), "--skip-commits")
	require.NoError(t, err)

	// Check that commits were not indexed
	commit, err := c.GetCommit(headSHA)
	require.Error(t, err)
	require.Empty(t, commit)

	// Check that blobs are indexed
	wikiBlob, err := c.GetWikiBlob("README.md")
	require.NoError(t, err)
	require.True(t, wikiBlob.Found)
	require.Equal(t, "p_"+projectIDString+"_README.md", wikiBlob.Id)
	require.Equal(t, "n_"+namespaceIdString, wikiBlob.Routing)

	blobDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(wikiBlob.Source, &blobDoc))
	require.Equal(
		t,
		map[string]interface{}{
			"type":           "wiki_blob",
			"language":       "Markdown",
			"path":           "README.md",
			"file_name":      "README.md",
			"oid":            "faaf198af3a36dbf41961466703cc1d47c61d051",
			"rid":            fmt.Sprintf("wiki_project_%s", projectIDString),
			"commit_sha":     headSHA,
			"content":        "testme\n======\n\nSample repo for testing gitlab features\n",
			"schema_version": schemaVersionWiki,
			"group_id":       float64(groupID),
			"project_id":     float64(projectID),
			"traversal_ids":  traversalId,
		},
		blobDoc,
	)
}

func TestIndexingProjectWikiBlobsWithArchivedFlagFalse(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	err, _ := run("", headSHA, "--blob-type=wiki_blob", groupIdOption(), projectIdOption(), archivedOption(false), schemaVersionWikiValueOption(), "--skip-commits")
	require.NoError(t, err)

	// Check that commits were not indexed
	commit, err := c.GetCommit(headSHA)
	require.Error(t, err)
	require.Empty(t, commit)

	// Check that blobs are indexed
	wikiBlob, err := c.GetWikiBlob("README.md")
	require.NoError(t, err)
	require.True(t, wikiBlob.Found)
	require.Equal(t, "p_"+projectIDString+"_README.md", wikiBlob.Id)
	require.Equal(t, "n_"+namespaceIdString, wikiBlob.Routing)

	blobDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(wikiBlob.Source, &blobDoc))
	require.Equal(
		t,
		map[string]interface{}{
			"type":           "wiki_blob",
			"language":       "Markdown",
			"path":           "README.md",
			"file_name":      "README.md",
			"oid":            "faaf198af3a36dbf41961466703cc1d47c61d051",
			"rid":            fmt.Sprintf("wiki_project_%s", projectIDString),
			"commit_sha":     headSHA,
			"content":        "testme\n======\n\nSample repo for testing gitlab features\n",
			"schema_version": schemaVersionWiki,
			"group_id":       float64(groupID),
			"project_id":     float64(projectID),
			"archived":       false,
			"traversal_ids":  traversalId,
		},
		blobDoc,
	)
}

func TestIndexingProjectWikiBlobsWithArchivedFlagTrue(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "project")
	defer td()

	err, _ := run("", headSHA, "--blob-type=wiki_blob", groupIdOption(), projectIdOption(), archivedOption(true), schemaVersionWikiValueOption(), "--skip-commits")
	require.NoError(t, err)

	// Check that commits were not indexed
	commit, err := c.GetCommit(headSHA)
	require.Error(t, err)
	require.Empty(t, commit)

	// Check that blobs are indexed
	wikiBlob, err := c.GetWikiBlob("README.md")
	require.NoError(t, err)
	require.True(t, wikiBlob.Found)
	require.Equal(t, "p_"+projectIDString+"_README.md", wikiBlob.Id)
	require.Equal(t, "n_"+namespaceIdString, wikiBlob.Routing)

	blobDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(wikiBlob.Source, &blobDoc))
	require.Equal(
		t,
		map[string]interface{}{
			"type":           "wiki_blob",
			"language":       "Markdown",
			"path":           "README.md",
			"file_name":      "README.md",
			"oid":            "faaf198af3a36dbf41961466703cc1d47c61d051",
			"rid":            fmt.Sprintf("wiki_project_%s", projectIDString),
			"commit_sha":     headSHA,
			"content":        "testme\n======\n\nSample repo for testing gitlab features\n",
			"schema_version": schemaVersionWiki,
			"group_id":       float64(groupID),
			"project_id":     float64(projectID),
			"archived":       true,
			"traversal_ids":  traversalId,
		},
		blobDoc,
	)
}

func TestIndexingGroupWikiBlobs(t *testing.T) {
	checkDeps(t)
	ensureGitalyRepository(t)
	c, td := buildWorkingIndex(t, "group")
	defer td()

	err, _ := run("", headSHA, "--blob-type=wiki_blob", groupIdOption(), schemaVersionWikiValueOption(), "--skip-commits")
	require.NoError(t, err)

	// Check that commits were not indexed
	commit, err := c.GetCommit(headSHA)
	require.Error(t, err)
	require.Empty(t, commit)

	// Check that blobs are indexed
	wikiBlob, err := c.GetWikiBlob("README.md")
	require.NoError(t, err)
	require.True(t, wikiBlob.Found)
	require.Equal(t, "g_"+groupIDString+"_README.md", wikiBlob.Id)
	require.Equal(t, "n_"+namespaceIdString, wikiBlob.Routing)

	blobDoc := make(map[string]interface{})
	require.NoError(t, json.Unmarshal(wikiBlob.Source, &blobDoc))
	require.Equal(
		t,
		map[string]interface{}{
			"type":           "wiki_blob",
			"language":       "Markdown",
			"path":           "README.md",
			"file_name":      "README.md",
			"oid":            "faaf198af3a36dbf41961466703cc1d47c61d051",
			"rid":            fmt.Sprintf("wiki_group_%s", groupIDString),
			"commit_sha":     headSHA,
			"content":        "testme\n======\n\nSample repo for testing gitlab features\n",
			"schema_version": schemaVersionWiki,
			"traversal_ids":  traversalId,
			"group_id":       float64(groupID),
		},
		blobDoc,
	)
}
